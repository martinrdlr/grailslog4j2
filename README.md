# Grails 4 project configured with Log4j2

Upwork task: "I have a project that currently uses a Grails 4 REST API and the default Grails Logback library. I have a new requirement that it must run on a Wildfly 26+ application server but chokes due to dependency issues. I'd like to remove the default Logback logging and replace it with Log4j2."

Requirements:

- [x] Create a blank Grails 4.0.9 project
- [x] Add the log4j2 dependencies and remove default Logback logging
- [ ] Recreate the settings (Pattern, File Rolling, etc) of my logback.groovy settings file into the proper Log4j2 settings. Prefereably in Groovy
- [x] Must successfully deploy to WildFly 26+
