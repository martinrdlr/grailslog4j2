package log4j2test

import grails.core.GrailsApplication
import grails.plugins.*
import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.LogManager

class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager
    
    Logger logger = LogManager.getRootLogger()

    def index() {
        logger.info "Hello world"
        [grailsApplication: grailsApplication, pluginManager: pluginManager]
    }
}
